package com.epam.controller;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import  com.epam.exceptions.*;
import com.epam.model.StudentModel;
import com.epam.view.StudentView;

public class StudentController {
    private StudentModel student;
    private StudentView view;

    public StudentController(StudentModel aModel, StudentView aView){
        this.student=aModel;
        this.view = aView;
    }
    public void setStudentName(String name) throws InvalidStudentNameException {
        student.setStudentName(name);
    }

    public String getStudentName(){
        return student.getStudentName();
    }

    public int getStudentId(){
        return student.getStudentID();
    }

    public double getStudentMark(){
        return student.getAverageMark();
    }

    public void updateView(){
        view.printInfo(student.getStudentName(),
                student.getStudentID(),getStudentMark());
    }
    public void saveInfo(){
        try(BufferedWriter buffWriter = new BufferedWriter(new FileWriter("students.txt",true))){
            if (getStudentName() != null) {
                buffWriter.write("\n");
                buffWriter.write(getStudentName()+"\n");
                buffWriter.write(getStudentId()+"\n");
                buffWriter.write(getStudentMark()+"\n");
            }
        }
        catch(IOException e){
            System.out.println("Cant write to file");
        }
    }
}
