package com.epam.view;

public class StudentView {
    public void printInfo(String aName, int anID, double aGrade){
        System.out.println("Student's name: "+ aName);
        System.out.println("Student's ID: "+ anID);
        System.out.println("Average mark: "+ aGrade);
    }
}
