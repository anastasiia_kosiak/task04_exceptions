package com.epam.exceptions;

public class InvalidStudentGradeException extends NumberFormatException {
    public InvalidStudentGradeException(String errorMessage) {
        super(errorMessage);
    }
}
