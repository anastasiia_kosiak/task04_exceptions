package com.epam;

import com.epam.view.StudentView;
import com.epam.model.StudentModel;
import com.epam.exceptions.*;
import com.epam.controller.StudentController;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Application {
    public static void main(String[] args) {
        List<StudentModel> studentList = new LinkedList<>();
        StudentView view = new StudentView();
        StudentController controller = null;

        for (int i = 1; i <= 10; i++) {
            StudentModel model = getStudentData(i);
            controller = new StudentController(model, view);
            controller.updateView();

            try {
                controller.setStudentName("Ana " + i);
                controller.saveInfo();
            } catch (InvalidStudentNameException e) {
                System.out.println("Invalid name");
            }
            controller.updateView();
        }
    }

    private static StudentModel getStudentData(int n) {
        Random rand = new Random();
        StudentModel student = new StudentModel();
        student.setStudentName("Ana "+ n);
        student.setStudentID(n);
        student.setAverageMark(rand.nextInt(4)*0.9 + n);
        return student;
    }
}

