package com.epam.model;
import com.epam.exceptions.InvalidStudentGradeException;

public class StudentModel {
    private int studentID;
    private String studentName;
    private double averageMark;

    public int getStudentID() {
        return studentID;
    }

    public void setStudentID(int studentID) {
        this.studentID = studentID;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public double getAverageMark() {
        return averageMark;
    }

    public void setAverageMark(double averageMark) {
        if(averageMark>0.0) {
            this.averageMark = averageMark;
        } else {
            throw new InvalidStudentGradeException("Average mark should " +
                    "be greater than zero.");
        }
    }

}
